﻿class ThrottlingPost {
    // Отправление запросов, когда они приходят ЧАЩЕ, чем возвращаются ответы
    // (Если ответ НЕ УСПЕЛ быть принятым, а уже подошли новые запросы,
    // то последний запрос будет сохранен и отправлен после того, как будет принят ответ на текущий)

    constructor(_url, _onAnswer, _onError) {
        /**
         * @param {String} _url - адрес, куда отсылать запросы
         * @param {Function}_onAnswer - обработчик события приема данных (ответ в текстовом виде)
         * @param {Function} _onError - обработчик события ошибки запроса
         */

        this.needUpdate = false;
        this.updating = false;

        this.url = _url;
        this.onAnswer = _onAnswer;
        this.onError = _onError;
    }

    get(_data, _answerType = "json") {

        let url = new URL(this.url);
        for (const key in _data) {
            if (_data.hasOwnProperty(key)) {
                url.searchParams.append(key, _data[key]);
            }
        }
        this.postRequest(url, _answerType);
    }

    postJson(_json, _answerType = "json") {
        /** Отправка JSON
         * @param _json - данные для отправки
         * @param _answerType - как интерпретировать ответ на запрос ('text'/'json'/...)
         */

        this.post(_json, "application/json", _answerType);
    }

    postFormData(_data, _answerType = "json") {
        /** Отправка FormData
         * @param _data - данные для отправки
         * @param _answerType - как интерпретировать ответ на запрос ('text'/'json'/...)
         */

        this.post(_data, null, _answerType);
    }

    post(_data, _contentType = null, _answerType = "json") {

        /** Отправка и прием данных на сервер с ожиданием ответа
         * @param _data - данные для отправки
         * @param _contentType - тип отправляемых на сервер данных, в большинстве случаев не надо задавать (кроме json)
         *                       null - если отправляется форма еще с файлами
         *                       'application/x-www-form-urlencoded'
         *                       'application/json' - если json
         * @param _answerType - как интерпретировать ответ на запрос ('text'/'json'/...)
         */

        // Подготовить запрос
        let headers;
        if (_contentType) {
            headers = {
                'Content-Type': _contentType
            };
        }
        let req = new Request(this.url, {
            method: "POST",
            body: _data,
            headers
        });

        this.postRequest(req, _answerType);
    }

    postRequest(_request, _answerType = "json") {
        /** Отправка и прием данных на сервер с ожиданием ответа
         * @param _request - все данные запроса
         * @param _answerType - как интерпретировать ответ на запрос ('text'/'json'/...)
         */

        // Сохранить данные последнего запроса
        this.request = _request;
        this.answerType = _answerType;

        // Если предыдущий запрос не был завершен, то дождаться его завершения
        if (this.updating) {
            this.needUpdate = true;
            return;
        }
        this.updating = true;

        fetch(this.request)
            .then(response => {
                if (response.ok) {
                    if (this.answerType === "text") {
                        return response.text();
                    } else {
                        return response.json();
                    }
                }
                throw new Error(`Response from ${this.url} is not Ok!`);
            })
            .then(resp => {
                this.updating = false;
                if (typeof this.onAnswer === "function") {
                    this.onAnswer(resp);
                }

                if (this.needUpdate) {
                    // Отправить запрос, если во время выполнения появилась необходимость нового запроса
                    this.needUpdate = false;
                    this.postRequest(this.request, this.answerType);
                }
            })
            .catch(response => {
                this.updating = false;
                this.needUpdate = false;
                if (typeof this.onError === "function") {
                    this.onError(response);
                }
            });
    }

    postRequestBatch(_request, _answerType = "json") {
        /** Отправка и прием данных на сервер с ожиданием ответа
         * @param _request - массив запросов 
         * @param _answerType - как интерпретировать ответ на запрос ('text'/'json'/...)
         */

        // Сохранить данные последнего запроса
        this.request = _request;
        this.answerType = _answerType;

        // Если предыдущий запрос не был завершен, то дождаться его завершения
        if (this.updating) {
            this.needUpdate = true;
            return;
        }
        this.updating = true;

        const rqs = _request.map(async _r => {
            return await fetch(_r)
                .then(response => {
                    if (response.ok) {
                        if (this.answerType === "text") {
                            return response.text();
                        } else {
                            return response.json();
                        }
                    }
                    throw new Error(`Response from ${_r} is not Ok!`);
                });
        });
        Promise.all(rqs)
            .then(
                resp => {
                    this.updating = false;
                    if (typeof this.onAnswer === "function") {
                        this.onAnswer(resp, _request);
                    }

                    if (this.needUpdate) {
                        // Отправить запрос, если во время выполнения появилась необходимость нового запроса
                        this.needUpdate = false;
                        this.postRequestBatch(this.request, this.answerType);
                    }
                }
            )
            .catch(response => {
                this.updating = false;
                this.needUpdate = false;
                if (typeof this.onError === "function") {
                    this.onError(response);
                }
            });
    }

}

export default ThrottlingPost;